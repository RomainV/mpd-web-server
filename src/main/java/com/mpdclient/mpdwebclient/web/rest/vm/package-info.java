/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mpdclient.mpdwebclient.web.rest.vm;
