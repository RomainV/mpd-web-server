package com.mpdclient.mpdwebclient.web.rest;

import com.mpdclient.mpdwebclient.mpdprotocol.MpdCommandEnum;
import com.mpdclient.mpdwebclient.mpdprotocol.MpdService;
import com.mpdclient.mpdwebclient.mpdprotocol.model.request.MpdRequest;
import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
@CrossOrigin()
public class WebsocketController {

    @Autowired
    private SimpMessagingTemplate template;

    private final MpdService mpd;

    public WebsocketController(MpdService pMpd) {
        this.mpd = pMpd;
    }

    @MessageMapping("/news")
    @SendTo("/topic/news")
    public MpdResponse<?> broadcastNews(@Payload MpdRequest message) {
        return mpd.sendRequest(message);
    }

    @Scheduled(fixedDelay = 1000)
    public void getStatus() {
        MpdRequest request = new MpdRequest(MpdCommandEnum.STATUS);
        MpdResponse<?> m = mpd.sendRequest(request);
        template.convertAndSend("/topic/news", m);
    }
}
