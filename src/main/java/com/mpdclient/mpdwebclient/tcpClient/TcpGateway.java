package com.mpdclient.mpdwebclient.tcpClient;


import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

@MessagingGateway
public interface TcpGateway {
    @Gateway(requestChannel = TcpServer.REQUEST_CHANNEL)
    void send(Message<String> pS);
}
