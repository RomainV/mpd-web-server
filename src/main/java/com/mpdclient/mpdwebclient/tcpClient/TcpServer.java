package com.mpdclient.mpdwebclient.tcpClient;

import com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.MpdDeserialiser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.ip.tcp.TcpOutboundGateway;
import org.springframework.integration.ip.tcp.connection.TcpNioClientConnectionFactory;
import org.springframework.messaging.PollableChannel;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class TcpServer {

    @Value("${mpd.server}")
    private String mpdHost;

    @Value("${mpd.port}")
    private Integer mpdPort;

    // Package private channel
    static final String REQUEST_CHANNEL = "request";

    private static final String REQUEST_TRANSFORMER = "transformer";
    public static final String RESPONSE_CHANNEL = "response";

    @Bean(name = RESPONSE_CHANNEL)
    public PollableChannel receiver() {
        return new QueueChannel();
    }

    @Bean(name = REQUEST_CHANNEL)
    public DirectChannel sender() {
        return new DirectChannel();
    }

    @Bean
    @ServiceActivator(inputChannel = REQUEST_CHANNEL)
    public TcpOutboundGateway mTcpOutboundGateway(TcpNioClientConnectionFactory pTcpNioClientCF) {
        TcpOutboundGateway lTcpOutboundGateway = new TcpOutboundGateway();
        lTcpOutboundGateway.setConnectionFactory(pTcpNioClientCF);
        lTcpOutboundGateway.setReplyChannelName( RESPONSE_CHANNEL );
        return lTcpOutboundGateway;
    }

    @Bean
    public TcpNioClientConnectionFactory clientConnectionFactory() {
        // Create socket factory
        TcpNioClientConnectionFactory factory = new TcpNioClientConnectionFactory(mpdHost, mpdPort);
        factory.setSingleUse(false); // IMPORTANT FOR SINGLE CHANNEL
        factory.setSoTimeout((int) TimeUnit.SECONDS.toMillis(50));
        factory.setDeserializer(new MpdDeserialiser());
        return factory;
    }
}
