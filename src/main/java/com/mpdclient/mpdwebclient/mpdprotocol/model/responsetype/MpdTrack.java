package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MpdTrack extends MpdObject {
    private String file;
    private LocalDateTime lastModified;
    private String artist;
    private String album;
    private String title;
    private String track;
    private LocalDate date;
    private String albumArtist;
    private Integer time;
    private Double duration;
    private Integer pos;
    private Integer id;
    // On find
    private String format;
    private String genre;

    public MpdTrack() {
        this.setType("track");
    }

    public String getFile() {
        return file;
    }

    public void setFile(String pFile) {
        file = pFile;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime pLastModified) {
        lastModified = pLastModified;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String pArtist) {
        artist = pArtist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String pAlbum) {
        album = pAlbum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String pTitle) {
        title = pTitle;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String pTrack) {
        track = pTrack;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate pDate) {
        date = pDate;
    }

    public String getAlbumArtist() {
        return albumArtist;
    }

    public void setAlbumArtist(String pAlbumArtist) {
        albumArtist = pAlbumArtist;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer pTime) {
        time = pTime;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double pDuration) {
        duration = pDuration;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pPos) {
        pos = pPos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String pFormat) {
        format = pFormat;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String pGenre) {
        genre = pGenre;
    }
}
