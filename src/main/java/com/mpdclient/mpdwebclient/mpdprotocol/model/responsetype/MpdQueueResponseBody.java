package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdOkListResponseBody;


public class MpdQueueResponseBody extends MpdOkListResponseBody {

    @Override
    public void addObjectIfNeeded(String pType) {
        if ( pType.contains("file")) {
            lastObject = new MpdTrack();
            data.add(lastObject);
        }
    }

}
