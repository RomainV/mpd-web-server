package com.mpdclient.mpdwebclient.mpdprotocol.model.response;

public class MpdResponse<T> {
    MpdResponseType mResponseType;
    protected T mpdBody;

    public MpdResponseType getResponseType() {
        return mResponseType;
    }

    public T getMpdBody() {
        return mpdBody;
    }

    public void setMpdBody(T info)  {
        mpdBody = info;
    }
}
