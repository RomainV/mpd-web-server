package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdOkListResponseBody;


public class MpdBrowseResponseBody extends MpdOkListResponseBody {

    public MpdBrowseResponseBody() {
    }

    @Override
    public void addObjectIfNeeded(String pType) {
        if  (pType.contains("directory")) {
            this.lastObject = new MpdFile();
            this.data.add(this.lastObject);
        } else if ( pType.contains("file")) {
            this.lastObject = new MpdTrack();
            this.data.add(this.lastObject);
        }
    }

    @Override
    public MpdObject getLastObject() {
        return lastObject;
    }
}
