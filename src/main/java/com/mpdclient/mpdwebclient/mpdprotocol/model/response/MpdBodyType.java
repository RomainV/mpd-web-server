package com.mpdclient.mpdwebclient.mpdprotocol.model.response;

public enum MpdBodyType {
    STATUS,
    QUEUE,
    SEARCH,
    ADD,
}
