package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

public class MpdObject {
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String pType) {
        type = pType;
    }
}
