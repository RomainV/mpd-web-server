package com.mpdclient.mpdwebclient.mpdprotocol.model.response;

public class MpdOkResponse extends MpdResponse<MpdOkResponseBody> {
    public MpdOkResponse(MpdOkResponseBody pResponseBody) {
        this.mResponseType = MpdResponseType.OK;
        this.mpdBody = pResponseBody;
    }
}
