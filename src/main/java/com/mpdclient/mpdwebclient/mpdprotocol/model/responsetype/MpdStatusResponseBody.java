package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdOkResponseBody;

public class MpdStatusResponseBody extends MpdOkResponseBody {
    private int volume;
    private int repeat;
    private int random;
    private int single;
    private int consume;
    private int playlist;
    private int playlistlenght;
    private double mixrampdb;
    private int mixrampdelay;
    private String state;
    private String song;
    private int songId;
    private String nextSong;
    private int nextSongId;
    private String time;
    private double elapse;
    private double duration;
    private int bitrate;
    private String xFade;
    private String audio;
    private int updatingDb;
    private String error;

    public MpdStatusResponseBody() {
    }

    private void setSong(String pValue) {
        this.song = pValue;
    }

    public int getRepeat() {
        return repeat;
    }

    public void setRepeat(int pRepeat) {
        repeat = pRepeat;
    }

    public int getRandom() {
        return random;
    }

    public void setRandom(int pRandom) {
        random = pRandom;
    }

    public int getSingle() {
        return single;
    }

    public void setSingle(int pSingle) {
        single = pSingle;
    }

    public int getConsume() {
        return consume;
    }

    public void setConsume(int pConsume) {
        consume = pConsume;
    }

    public int getPlaylist() {
        return playlist;
    }

    public void setPlaylist(int pPlaylist) {
        playlist = pPlaylist;
    }

    public int getPlaylistlenght() {
        return playlistlenght;
    }

    public void setPlaylistlenght(int pPlaylistlenght) {
        playlistlenght = pPlaylistlenght;
    }

    public double getMixrampdb() {
        return mixrampdb;
    }

    public void setMixrampdb(double pMixrampdb) {
        mixrampdb = pMixrampdb;
    }

    public String getState() {
        return state;
    }

    public void setState(String pState) {
        state = pState;
    }

    public String getSong() {
        return song;
    }

    public int getSongId() {
        return songId;
    }

    public void setSongId(int pSongId) {
        songId = pSongId;
    }

    public String getNextSong() {
        return nextSong;
    }

    public void setNextSong(String pNextSong) {
        nextSong = pNextSong;
    }

    public int getNextSongId() {
        return nextSongId;
    }

    public void setNextSongId(int pNextSongId) {
        nextSongId = pNextSongId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String pTime) {
        time = pTime;
    }

    public double getElapse() {
        return elapse;
    }

    public void setElapse(double pElapse) {
        elapse = pElapse;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double pDuration) {
        duration = pDuration;
    }

    public int getBitrate() {
        return bitrate;
    }

    public void setBitrate(int pBitrate) {
        bitrate = pBitrate;
    }

    public String getXFade() {
        return xFade;
    }

    public void setXFade(String pXFade) {
        xFade = pXFade;
    }

    public double getMixrampdelay() {
        return mixrampdelay;
    }

    public void setMixrampdelay(int pMixrampdelay) {
        mixrampdelay = pMixrampdelay;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String pAudio) {
        audio = pAudio;
    }

    public int getUpdatingDb() {
        return updatingDb;
    }

    public void setUpdatingDb(int pUpdatingDb) {
        updatingDb = pUpdatingDb;
    }

    public String getError() {
        return error;
    }

    public void setError(String pError) {
        error = pError;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int pVolume) {
        volume = pVolume;
    }
}
