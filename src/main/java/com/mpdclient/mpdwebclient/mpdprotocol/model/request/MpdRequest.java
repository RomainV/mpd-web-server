package com.mpdclient.mpdwebclient.mpdprotocol.model.request;

import com.mpdclient.mpdwebclient.mpdprotocol.MpdCommandEnum;

import java.util.List;

public class MpdRequest {
    MpdCommandEnum command;
    List<String> args;

    public MpdRequest() {
    }

    public MpdRequest(MpdCommandEnum pCommand) {
        command = pCommand;
    }

    public MpdCommandEnum getCommand() {
        return command;
    }

    public void setCommand(MpdCommandEnum pCommand) {
        command = pCommand;
    }

    public List<String> getArgs() {
        return args;
    }

    public void setArgs(List<String> pArgs) {
        args = pArgs;
    }
}
