package com.mpdclient.mpdwebclient.mpdprotocol.model.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype.MpdObject;

import java.util.ArrayList;
import java.util.List;

public abstract class MpdOkListResponseBody extends MpdOkResponseBody {
    protected List<MpdObject> data = new ArrayList<>();

    @JsonIgnore
    protected MpdObject lastObject;

    public abstract void addObjectIfNeeded(String pType);

    public MpdObject getLastObject() {
        return lastObject;
    }

    public List<MpdObject> getData() {
        return data;
    }

    public void setData(List<MpdObject> pData) {
        data = pData;
    }
}
