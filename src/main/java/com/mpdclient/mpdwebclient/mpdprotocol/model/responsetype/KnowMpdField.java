package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

public enum KnowMpdField {
    // Commun
    TIME("Time", "time", MpdFieldType.STRING),
    DURATION("duration", "duration", MpdFieldType.STRING),
    // Status field
    REPEAT("repeat", "repeat", MpdFieldType.INT),
    RANDOM("random", "random", MpdFieldType.INT),
    SINGLE("single", "single", MpdFieldType.INT),
    CONSUME("consume", "consume", MpdFieldType.INT),
    VOLUME("volume", "volume", MpdFieldType.INT),
    PLAYLIST("playlist", "playlist", MpdFieldType.INT),
    PLAYLISTLENGHT("playlistlenght", "playlistlenght", MpdFieldType.INT),
    MIXRAMPDB("mixrampdb", "mixrampdb", MpdFieldType.DOUBLE),
    MIXRAMPDELAY("mixrampdelay", "mixrampdelay", MpdFieldType.INT),
    STATE("state", "state", MpdFieldType.STRING),
    SONG("song", "song", MpdFieldType.STRING),
    NGID("songid", "songId", MpdFieldType.INT),
    NEXTSONG("nextsong", "nextSong", MpdFieldType.STRING),
    NEXTSONGID("nextsongid", "nextSongId", MpdFieldType.INT),
    ELAPSE("elapsed", "elapse", MpdFieldType.DOUBLE),
    BITRATE("bitrate", "bitrate", MpdFieldType.INT),
    XFADE("xfade", "xFade", MpdFieldType.STRING),
    AUDIO("audio", "audio", MpdFieldType.STRING),
    UPDATINGDB("updating_db", "updatingDb", MpdFieldType.INT),
    ERROR("error", "error", MpdFieldType.STRING),
    // File field
    FILE("file", "file", MpdFieldType.STRING),
    LAST_MODIFIED("Last-Modified", "Last-Modified", MpdFieldType.DATETIMEISO),
    ARTIST("Artist", "artist", MpdFieldType.STRING),
    ALBUM("Album", "album", MpdFieldType.STRING),
    TITLE("Title", "title", MpdFieldType.STRING),
    TRACK("Track", "track", MpdFieldType.STRING),
    DATE("Date", "date", MpdFieldType.DATEISO),
    ALBUMARTIST("AlbumArtist", "albumArtist", MpdFieldType.STRING),
    POS("Pos", "pos", MpdFieldType.STRING),
    ID("Id", "id", MpdFieldType.STRING),
    FORMAT("Format", "format", MpdFieldType.STRING),
    GENRE("Genre", "genre", MpdFieldType.STRING),
    // Directories
    DIRECTORY("directory", "directory", MpdFieldType.STRING),
    // Binary
    BINARY_LENGHT("binaryLength", "binaryLength", MpdFieldType.INT),
    BINARY("binary", "binary", MpdFieldType.STRING),
    SIZE("size", "size", MpdFieldType.INT),
    TYPE("type", "type", MpdFieldType.STRING);


    private final String mMpdName;
    private final String mFieldFriendlyName;
    private final MpdFieldType mFieldType;

    KnowMpdField(String mpdName, String fieldFriendlyName, MpdFieldType pFieldType) {
        mMpdName = mpdName;
        mFieldFriendlyName = fieldFriendlyName;
        mFieldType = pFieldType;
    }

    public String getMpdName() {
        return mMpdName;
    }

    public String getFieldFriendlyName() {
        return mFieldFriendlyName;
    }

    public static KnowMpdField reverseSearch(String pMpdName) {
        for ( KnowMpdField field : KnowMpdField.values() ) {
            if ( field.getMpdName().equals(pMpdName)) {
                return field;
            }
        }
        throw new IllegalArgumentException();
    }

    public MpdFieldType getFieldType() {
        return mFieldType;
    }
}
