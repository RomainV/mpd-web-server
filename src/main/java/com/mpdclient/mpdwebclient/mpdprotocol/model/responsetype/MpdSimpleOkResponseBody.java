package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

import com.mpdclient.mpdwebclient.mpdprotocol.MpdCommandEnum;
import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdOkResponseBody;

public class MpdSimpleOkResponseBody extends MpdOkResponseBody {

    public MpdSimpleOkResponseBody() {

    }

    public MpdSimpleOkResponseBody(MpdCommandEnum pMpdCommandEnum) {
        this.setResponseType(pMpdCommandEnum);
    }
}
