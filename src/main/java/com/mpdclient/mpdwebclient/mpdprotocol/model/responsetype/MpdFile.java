package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

import java.time.LocalDateTime;

public class MpdFile extends MpdObject {
    public MpdFile() {
        this.setType("file");
    }

    private String directory;
    private LocalDateTime lastModified;

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String pDirectory) {
        directory = pDirectory;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime pLastModified) {
        lastModified = pLastModified;
    }
}
