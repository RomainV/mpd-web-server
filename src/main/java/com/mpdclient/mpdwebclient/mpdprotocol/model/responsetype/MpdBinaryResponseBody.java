package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdOkResponseBody;

public class MpdBinaryResponseBody extends MpdOkResponseBody {
    private int binaryLength;
    private String binary;
    private int size;
    private String type;

    public MpdBinaryResponseBody() {
    }

    public int getBinaryLength() {
        return binaryLength;
    }

    public void setBinaryLength(int pBinaryLength) {
        binaryLength = pBinaryLength;
    }

    public String getBinary() {
        return binary;
    }

    public void setBinary(String pBinary) {
        binary = pBinary;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int pSize) {
        size = pSize;
    }

    public String getType() {
        return type;
    }

    public void setType(String pType) {
        type = pType;
    }
}
