package com.mpdclient.mpdwebclient.mpdprotocol.model.response;

import com.mpdclient.mpdwebclient.mpdprotocol.MpdCommandEnum;

public class MpdOkResponseBody {
    private MpdCommandEnum responseType;

    public MpdCommandEnum getResponseType() {
        return responseType;
    }

    public void setResponseType(MpdCommandEnum pResponseType) {
        responseType = pResponseType;
    }
}
