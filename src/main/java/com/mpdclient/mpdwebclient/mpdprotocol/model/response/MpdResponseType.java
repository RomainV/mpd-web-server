package com.mpdclient.mpdwebclient.mpdprotocol.model.response;

public enum MpdResponseType {
    OK,
    ACK,
    ERROR,
}
