package com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype;

public enum MpdFieldType {
    STRING,
    DATEISO,
    DATETIMEISO,
    INT,
    DOUBLE
}
