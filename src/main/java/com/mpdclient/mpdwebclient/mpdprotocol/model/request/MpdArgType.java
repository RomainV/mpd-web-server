package com.mpdclient.mpdwebclient.mpdprotocol.model.request;

public enum MpdArgType {
    STRING,
    INTEGER,
    EXPRESSION,
}
