package com.mpdclient.mpdwebclient.mpdprotocol;

import com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.fields.MpdField;
import com.mpdclient.mpdwebclient.mpdprotocol.factory.MpdCommandFactory;
import com.mpdclient.mpdwebclient.mpdprotocol.model.request.MpdRequest;
import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdResponse;
import com.mpdclient.mpdwebclient.mpdprotocol.parser.MpdParser;
import com.mpdclient.mpdwebclient.tcpClient.TcpGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;
import org.springframework.messaging.PollableChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import static com.mpdclient.mpdwebclient.tcpClient.TcpServer.RESPONSE_CHANNEL;

@Service
public class MpdService {

    @Autowired
    MpdCommandFactory mMpdCommandFactory;

    @Autowired
    TcpGateway mTcpGateway;

    @Autowired
    MpdParser mMpdParser;

    @Autowired
    @Qualifier(RESPONSE_CHANNEL)
    PollableChannel receiver;

    public MpdResponse<?> sendRequest(MpdRequest request) {
        String command = this.mMpdCommandFactory.getCommand(request.getCommand(), request.getArgs());
        this.mTcpGateway.send(MessageBuilder.withPayload(command).build());
        Message<?> message = receiver.receive(50);
        if (message != null) {
            ArrayList<MpdField> mpdResp = (ArrayList<MpdField>) message.getPayload();
            return this.mMpdParser.parse(mpdResp, request.getCommand());
        }
        return null;
    }
}
