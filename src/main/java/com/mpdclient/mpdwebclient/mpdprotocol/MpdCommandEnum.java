package com.mpdclient.mpdwebclient.mpdprotocol;

import com.mpdclient.mpdwebclient.mpdprotocol.model.request.MpdArgType;

public enum MpdCommandEnum {
    STATUS("status" ),
    QUEUE("playlistinfo"),
    SEARCH("search", MpdArgType.EXPRESSION),
    ADD("add", MpdArgType.STRING),
    PAUSE("pause", MpdArgType.INTEGER),
    NEXT("next"),
    PREVIOUS("previous"),
    DELETE("delete", MpdArgType.INTEGER),
    PLAY("play", MpdArgType.INTEGER),
    BROWSE("lsinfo", MpdArgType.STRING),
    VOLUME("setvol", MpdArgType.INTEGER),
    MOVE("move", MpdArgType.INTEGER, MpdArgType.INTEGER),
    CLEAR("clear"),
    ALBUMART("readpicture", MpdArgType.STRING, MpdArgType.INTEGER),
    ;

    private final String command;
    private MpdArgType[] args;

    MpdCommandEnum(String pCommand, MpdArgType... pArgs) {
        this.command = pCommand;
        this.args = pArgs;
    }

    public String getCommand() {
        return command;
    }

    public MpdArgType[] getArgs() {
        return args;
    }

    public void setArgs(MpdArgType[] pArgs) {
        this.args = pArgs;
    }
}
