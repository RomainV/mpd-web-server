package com.mpdclient.mpdwebclient.mpdprotocol.parser;

import com.mpdclient.mpdwebclient.mpdprotocol.MpdCommandEnum;
import com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.fields.MpdField;
import com.mpdclient.mpdwebclient.mpdprotocol.error.CannotParseResponseException;
import com.mpdclient.mpdwebclient.mpdprotocol.factory.MpdResponseFactory;
import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdErrorResponse;
import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdResponse;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MpdParser {

    private final MpdResponseFactory mMpdResponseFactory;

    public MpdParser(
        MpdResponseFactory pMpdResponseFactory
    ) {
        mMpdResponseFactory = pMpdResponseFactory;
    }

    public MpdResponse<?> parse(ArrayList<MpdField> mpdResp, MpdCommandEnum typeExpected) {
//        if ( mpdResp.size() == 1 && mpdResp.get(0).startsWith("ACK")) {
//            return new MpdAckResponse();
//        }
        try {
            return this.mMpdResponseFactory.createResponse(typeExpected, mpdResp);
        } catch (CannotParseResponseException pE) {
            return new MpdErrorResponse();
        }
    }
}
