package com.mpdclient.mpdwebclient.mpdprotocol.factory;

import com.mpdclient.mpdwebclient.mpdprotocol.MpdCommandEnum;
import com.mpdclient.mpdwebclient.mpdprotocol.model.request.MpdArgType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MpdCommandFactory {

    public String getCommand(MpdCommandEnum command, List<String> args ) {
        StringBuilder result = new StringBuilder(command.getCommand());

        assert args.size() == command.getArgs().length;
        for ( int argNumber = 0; argNumber < command.getArgs().length; argNumber++) {
            result.append(" ").append(
                this.processArg(
                    command.getArgs()[argNumber],
                    args.get(argNumber)
                )
            );
        }

        return result.toString();
    }

    private String processArg(MpdArgType pArg, String pS) {
        return switch (pArg) {
            case EXPRESSION, STRING -> " \"" + pS.replace("\"", "\\\"") + "\"";
            case INTEGER -> pS;
        };
    }
}
