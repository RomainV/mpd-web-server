package com.mpdclient.mpdwebclient.mpdprotocol.factory;

import com.mpdclient.mpdwebclient.mpdprotocol.MpdCommandEnum;
import com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.fields.MpdField;
import com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.fields.MpdFieldString;
import com.mpdclient.mpdwebclient.mpdprotocol.error.CannotParseResponseException;
import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdOkListResponseBody;
import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdOkResponse;
import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdOkResponseBody;
import com.mpdclient.mpdwebclient.mpdprotocol.model.response.MpdResponse;
import com.mpdclient.mpdwebclient.mpdprotocol.model.responsetype.*;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@Service
public class MpdResponseFactory {

    public MpdResponse<?> createResponse(MpdCommandEnum fromCommand, List<MpdField> fields) throws CannotParseResponseException {
        try {
            MpdOkResponseBody body = getResponseClassByCommandType(fromCommand)
                .getConstructor()
                .newInstance();
            populateBody(body, fromCommand, fields);

            return new MpdOkResponse(body);

        } catch (InstantiationException | IllegalAccessException |
                 InvocationTargetException | NoSuchMethodException pE) {
            throw new CannotParseResponseException();
        }
    }

    private void populateBody(MpdOkResponseBody pBody, MpdCommandEnum command, List<MpdField> pFields) {
        pBody.setResponseType(command);
        Object operateOn = pBody;
        for (MpdField field : pFields) {
            if (field instanceof MpdFieldString fieldString) {
                // If the body is a list, check object to populate
                if (pBody instanceof MpdOkListResponseBody) {
                    ((MpdOkListResponseBody) pBody).addObjectIfNeeded(fieldString.getFieldName());
                    operateOn = ((MpdOkListResponseBody) pBody).getLastObject();
                }

                try {
                    KnowMpdField fieldRecognise = KnowMpdField.reverseSearch(fieldString.getFieldName());
                    Object convertedValue = convertValue(fieldString.getValue(), fieldRecognise.getFieldType());
                    BeanUtils.setProperty(operateOn, fieldRecognise.getFieldFriendlyName(), convertedValue);
                } catch (IllegalAccessException | InvocationTargetException | IllegalArgumentException ignore) {
                    // Les champs qui ne peuvent pas être set sont ignoré
                }
            }
        }
    }

    private Class<? extends MpdOkResponseBody> getResponseClassByCommandType(MpdCommandEnum fromCommand) {
        return
            switch (fromCommand) {
            case BROWSE -> MpdBrowseResponseBody.class;
            case SEARCH -> MpdSearchResponseBody.class;
            case QUEUE -> MpdQueueResponseBody.class;
            case STATUS -> MpdStatusResponseBody.class;
            case ALBUMART -> MpdBinaryResponseBody.class;
            default -> MpdSimpleOkResponseBody.class;
        };
    }

    private Object convertValue(String pValue, MpdFieldType pFieldType) {
        switch (pFieldType) {
            case DATEISO:
                try{
                    return (LocalDateTime.parse(pValue, DateTimeFormatter.BASIC_ISO_DATE));
                } catch (DateTimeParseException ignore) { break; /* Simply ignore bad date */}
            case DATETIMEISO:
                try{
                    return (LocalDateTime.parse(pValue, DateTimeFormatter.ISO_DATE_TIME));
                } catch (DateTimeParseException ignore) { break; /* Simply ignore bad date */}
            case STRING:
                return pValue;
            case INT:
                return Integer.parseInt(pValue);
            case DOUBLE:
                return Double.parseDouble(pValue);
        }
        // An error appear ! Simply ignore the field, for now ?
        return null;
    }

}
