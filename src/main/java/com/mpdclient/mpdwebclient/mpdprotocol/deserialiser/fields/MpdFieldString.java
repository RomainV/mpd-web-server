package com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.fields;

public class MpdFieldString extends MpdField {
    private String fieldName;
    private String value;

    public MpdFieldString() {
    }

    public MpdFieldString(String pFieldName, String pValue) {
        fieldName = pFieldName;
        value = pValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String pFieldName) {
        fieldName = pFieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String pValue) {
        value = pValue;
    }
}
