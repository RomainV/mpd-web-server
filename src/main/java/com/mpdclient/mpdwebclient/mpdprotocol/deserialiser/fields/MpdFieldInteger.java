package com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.fields;

public class MpdFieldInteger extends MpdField {
    private String fieldName;
    private Integer value;

    public MpdFieldInteger() {
    }

    public MpdFieldInteger(String pFieldName, Integer pValue) {
        fieldName = pFieldName;
        value = pValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String pFieldName) {
        fieldName = pFieldName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Integer pValue) {
        value = pValue;
    }
}
