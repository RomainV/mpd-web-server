package com.mpdclient.mpdwebclient.mpdprotocol.deserialiser;

import com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.fields.MpdField;
import com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.fields.MpdFieldString;
import org.bouncycastle.util.Arrays;
import org.springframework.core.serializer.Deserializer;
import org.springframework.integration.ip.tcp.serializer.SoftEndOfStreamException;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Component
public class MpdDeserialiser implements Deserializer<List<MpdField>> {

    @Override
    public List<MpdField> deserialize( InputStream inputStream) throws IOException {
        List<MpdField> list = new ArrayList<>();
        while (true) {
            String line = this.getLine(inputStream);

            if (line.contains("binary")) {
                // gerer les images
                //      ie. binary: 68\nXXXX
                String[] fields = line.split(": ");
                Integer bytesNumber = Integer.parseInt(fields[1]);
                list.add(new MpdFieldString("binaryLength", bytesNumber.toString()));
                // getBinary
                byte[] bytes = new byte[0];
                while (bytesNumber > 0) {
                    bytesNumber -= 1;
                    int bite = inputStream.read();
                    if (bite < 0) {
                        throw new SoftEndOfStreamException("Stream closed between payloads");
                    }
                    bytes = Arrays.append(bytes, (byte) bite);
                }
                list.add(new MpdFieldString("binary", Base64.getEncoder().encodeToString(bytes)));
            } else if ( line.matches(".*: .*") ) {
                String[] fields = line.split(": ");
                list.add(new MpdFieldString(fields[0], fields[1]));
            }

            if ("OK".equals(line) || line.contains("ACK") || line.contains("OK MPD")) {
                break;
            }
        }
        return list;
    }

    private String getLine(@NonNull InputStream inputStream) throws IOException {
        byte[] bytes = new byte[0];

        while (true) {
            int bite = inputStream.read();
            if (bite < 0) {
                throw new SoftEndOfStreamException("Stream closed between payloads");
            }
            // Stop without adding at '\n'
            if (bite == 0x0A) {
                break;
            }
            bytes = Arrays.append(bytes, (byte) bite);
        }
        return new String(bytes, StandardCharsets.UTF_8);
    }
}
