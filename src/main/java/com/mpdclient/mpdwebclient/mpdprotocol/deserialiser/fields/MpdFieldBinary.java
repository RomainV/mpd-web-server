package com.mpdclient.mpdwebclient.mpdprotocol.deserialiser.fields;

public class MpdFieldBinary extends MpdField {
    private String fieldName;
    private Object value;

    public MpdFieldBinary() {
    }

    public MpdFieldBinary(String pFieldName, Object pValue) {
        fieldName = pFieldName;
        value = pValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String pFieldName) {
        fieldName = pFieldName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object pValue) {
        value = pValue;
    }
}
